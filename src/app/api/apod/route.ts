// app/api/apod/route.ts
import { NextRequest, NextResponse } from 'next/server';
import axios from 'axios';

const NASA_APOD_URL = 'https://api.nasa.gov/planetary/apod';
const API_KEY = process.env.API_KEY;

export async function GET(request: NextRequest) {
  try {
    const response = await axios.get(NASA_APOD_URL, {
      params: {
        api_key: API_KEY,
      },
    });

    return NextResponse.json(response.data);
  } catch (error) {
    console.error('Failed to fetch data from NASA APOD API', error);
    return new NextResponse(
      JSON.stringify({ error: 'Failed to fetch data from NASA APOD API' }),
      { status: 500, headers: { 'Content-Type': 'application/json' } }
    );
  }
}
