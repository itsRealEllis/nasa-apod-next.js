"use client";
import { useEffect, useState } from "react";
import Image from "next/image";
import { Staatliches, Poppins } from "next/font/google";
import { RiAccountCircleFill } from "react-icons/ri";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import ClockLoader from "react-spinners/ClockLoader";

const staatliches = Staatliches({ subsets: ["latin"], weight: "400" });
const poppins = Poppins({ subsets: ["latin"], weight: "300" });

interface ApodData {
  copyright: string;
  date: string;
  explanation: string;
  hdurl: string;
  media_type: string;
  service_version: string;
  title: string;
  url: string;
}

const Home = () => {
  const [data, setData] = useState<ApodData | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("/api/apod");
      const result = await response.json();
      setData(result);
    };

    fetchData();
  }, []);
  if (!data) {
    return (
      <div className="h-screen w-screen flex items-center justify-center bg-black text-white">
        <ClockLoader size={80} aria-label="Loading Spinner" color="#fff" />
      </div>
    );
  }

  return (
    <main>
      <div className="lg:w-1/3 w-[95%] mx-auto space-y-3 py-5 relative">
        <h1
          className={`lg:text-[3.5vw] text-[15vw] leading-[4rem] ${staatliches.className}`}
        >
          {data?.title ? data.title : <Skeleton />}
        </h1>
        {data?.title && data.media_type == "image" ? (
          <Image
            height={500}
            width={500}
            src={data.url}
            alt={data.title}
            loading="eager"
            className="w-full h-96 rounded-3xl object-cover"
          />
        ) : (
          <Skeleton height={384} className="rounded-3xl" />
        )}

        {data?.copyright ? (
          <p className="font-semibold">
            <RiAccountCircleFill className="inline mr-1 text-2xl" />{" "}
            {data.copyright}
          </p>
        ) : (
          <Skeleton />
        )}
        {data?.explanation ? (
          <p className={`text-base text-justify ${poppins.className}`}>
            {data.explanation}
          </p>
        ) : (
          <Skeleton count={10} />
        )}

        <div className="sticky bottom-0 flex justify-center items-center left-0 right-0 h-28 bg-gradient-to-b from-transparent via-black to-black">
          {data?.url ? (
            <Link
              href={data.url}
              className="mx-1 w-full relative font-bold rounded-full h-10 bg-white text-black text-center flex justify-center items-center"
            >
              Visit
            </Link>
          ) : (
            <Skeleton height={40} />
          )}
        </div>
      </div>
    </main>
  );
};

export default Home;
